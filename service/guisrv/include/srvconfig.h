#ifndef __GUISRV_CONFIG_H__
#define __GUISRV_CONFIG_H__

/* 图形界面服务配置头文件 */


/* 字体配置 */
#define CONFIG_FONT_STANDARD
#define CONFIG_FONT_SIMSUN

/* 配置高效的窗口移动 */
#define CONFIG_FAST_WINMOVE


#endif  /* __GUISRV_CONFIG_H__ */